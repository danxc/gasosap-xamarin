﻿using GasosApp.Models;
using GasosApp.Services;
using Xamarin.Forms;

namespace GasosApp.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public IDataStore<Item> ItemDataStore => DependencyService.Get<IDataStore<Item>>();
        public ItemDetailViewModel(Item item = null)
        {
            Title = item.Text;
            Item = item;
        }

        int quantity = 1;
        public int Quantity
        {
            get { return quantity; }
            set { SetProperty(ref quantity, value); }
        }
    }
}