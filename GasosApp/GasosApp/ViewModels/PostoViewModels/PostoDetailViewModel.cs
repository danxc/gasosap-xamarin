﻿using GasosApp.Models;
using GasosApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GasosApp.ViewModels.PostoViewModels
{
    public class PostoDetailViewModel:BaseViewModel
    {
        public Posto Posto { get; set; }
        public IDataStore<Posto> PostoDataStore => DependencyService.Get<IDataStore<Posto>>();
        public PostoDetailViewModel(Posto posto = null)
        {
            Title = posto.Nome;
            Posto = posto;
        }

        int quantity = 1;
        public int Quantity
        {
            get { return quantity; }
            set { SetProperty(ref quantity, value); }
        }
    }
}
