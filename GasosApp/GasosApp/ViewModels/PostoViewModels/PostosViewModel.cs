﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using GasosApp.Helpers;
using GasosApp.Models;
using GasosApp.Views;

using Xamarin.Forms;
using GasosApp.Views.PostosViews;
using GasosApp.Services;
using System.Windows.Input;

namespace GasosApp.ViewModels.PostoViewModels
{
    class PostosViewModel:BaseViewModel
    {
        public IDataStore<Posto> PostoDataStore => DependencyService.Get<IDataStore<Posto>>();
        public ObservableRangeCollection<Posto> Postos { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command DeletePosto { get; private set; }

        public PostosViewModel()
        {
            Title = "Lista de Postos";
            Postos = new ObservableRangeCollection<Posto>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            DeletePosto = new Command(async (a)=> await DelPosto(a));
            MessagingCenter.Subscribe<NewPostoPage, Posto>(this, "AddPosto", async (obj, posto) =>
            {
                var _posto = posto as Posto;
                Postos.Add(_posto);
                await PostoDataStore.AddItemAsync(_posto);
            });
        }

        async Task DelPosto(object posto)
        {
            var _posto = posto as Posto;
            Postos.Remove(_posto);
            await PostoDataStore.DeleteItemAsync(_posto);
        }
        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Postos.Clear();
                var postos = await PostoDataStore.GetItemsAsync(true);
                Postos.ReplaceRange(postos);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load Postos.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}

