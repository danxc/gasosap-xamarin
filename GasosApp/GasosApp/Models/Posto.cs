﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GasosApp.Models
{
    public class Posto:BaseDataObject
    {
        
        public string nome;
        public string Nome
        {
            get { return nome; }
            set { SetProperty(ref nome, value); }
        }

        public IDictionary<string, string> combustivel;
        public IDictionary<string, string> Combustivel
        {
            get { return combustivel; }
            set { SetProperty(ref combustivel, value); }
        }

        public string latitude;
        public string Latitude
        {
            get { return latitude; }
            set { SetProperty(ref latitude, value); }
        }

        public string longitude;
        public string Longitude
        {
            get { return longitude; }
            set { SetProperty(ref longitude, value); }
        }

        public string endereco;
        public string Endereco
        {
            get { return endereco; }
            set { SetProperty(ref endereco, value); }
        }


    }
}
