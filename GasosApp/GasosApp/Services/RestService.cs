﻿using GasosApp.Models;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Plugin.Geolocator;

using System.Threading.Tasks;

namespace GasosApp.Services
{
    class RestService
    {
        HttpClient client;

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public async Task<Posto> Create(Posto posto)
        {
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("nome", posto.Nome));
            postData.Add(new KeyValuePair<string, string>("endereco", posto.Endereco));

            var content = new FormUrlEncodedContent(postData);

            var response = await client.PostAsync("http://138.197.77.177:8005/api/web/posto", content);

            var JsonResult = response.Content.ReadAsStringAsync().Result;

            return null;
        }

        public async Task<List<Posto>> RefreshDataAsync()
        {
            
            
            

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            
            var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
            
        
            List<Posto> Postos = null;
            var model = "posto";
            var action = "";
            var id = "1";
            //string RestUrl = "http://138.197.77.177:8005/api/web/posto";
            string RestUrl = "http://192.168.1.13:8005/api/web/posto";
            Uri uri;
            if (position != null)
            {
                uri = new Uri(RestUrl + "?location=" + position.Latitude.ToString(System.Globalization.CultureInfo.InvariantCulture) + "," + position.Longitude.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }
            else
            {
                uri = new Uri(RestUrl + "?location=-12.9387385,-38.3534505");
            }
                
            try
            {
                var response = await client.GetAsync(uri);
            
            
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                
                Postos = JsonConvert.DeserializeObject<List<Posto>>(content);
            }
            }
            catch (Exception)
            {

                Debug.WriteLine("##### erro ######");
            }
            return Postos;

        }

    }
}
