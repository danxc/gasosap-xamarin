﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GasosApp.Models;

using Xamarin.Forms;
using System.Net.Http;

[assembly: Dependency(typeof(GasosApp.Services.PostoDataStore))]
namespace GasosApp.Services
{
    public class PostoDataStore : IDataStore<Posto>
    {
        bool isInitialized;
        List<Posto> postos;
       
        
        public async Task<bool> AddItemAsync(Posto posto)
        {
            await InitializeAsync();
            RestService _restService = new RestService();
            await _restService.Create(posto);
            postos.Add(posto);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Posto posto)
        {
            await InitializeAsync();

            var _posto = postos.Where((Posto arg) => arg.Id == posto.Id).FirstOrDefault();
            postos.Remove(_posto);
            postos.Add(posto);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(Posto posto)
        {
            await InitializeAsync();

            var _posto = postos.Where((Posto arg) => arg.Id == posto.Id).FirstOrDefault();
            postos.Remove(_posto);

            return await Task.FromResult(true);
        }

        public async Task<Posto> GetItemAsync(string id)
        {
            
            await InitializeAsync();

            return await Task.FromResult(postos.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Posto>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();

            return await Task.FromResult(postos);
        }

        public Task<bool> PullLatestAsync()
        {
            return Task.FromResult(true);
        }


        public Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }

        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;
            
            postos = new List<Posto>();
            RestService _restService = new RestService();
            var _postos =await  _restService.RefreshDataAsync();
            
            foreach (Posto posto in _postos)
            {
                postos.Add(posto);
            }

            isInitialized = true;
        }
    }
}
