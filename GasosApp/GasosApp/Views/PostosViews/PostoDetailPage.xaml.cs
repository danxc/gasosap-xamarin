﻿using GasosApp.ViewModels.PostoViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GasosApp.Views.PostosViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostoDetailPage : ContentPage
    {
        PostoDetailViewModel viewModel;
        public PostoDetailPage()
        {
            InitializeComponent();
            
        }

        public PostoDetailPage(PostoDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
            Picker picker = InitPicker();
            Entry valor = new Entry { Placeholder="Valor", Keyboard=Keyboard.Numeric};
            Button salvar = new Button { Text="Salvar"};
            salvar.Clicked += salvarPreco;
            layout.Children.Add(picker);
            layout.Children.Add(valor);
        }

        public void salvarPreco(object sender, EventArgs args)
        {

        }
        public Picker InitPicker()
        {
            Picker p = new Picker
            {
                Title = "Combustível",
                
            }; 
            Dictionary<int, string> combustiveis = new Dictionary<int, string>
            {
                { 1, "Gasolina"},
                { 2, "Álcool"},
                { 3, "Diesel"},
                { 4, "Gás"},
            };
            
            foreach(string combustivel in combustiveis.Values)
            {
                p.Items.Add(combustivel);
            }
            return p;
        }
    }
}
