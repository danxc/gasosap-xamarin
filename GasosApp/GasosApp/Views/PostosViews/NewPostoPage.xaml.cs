﻿using GasosApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GasosApp.Views.PostosViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewPostoPage : ContentPage
    {
        public Posto Posto { get; set; }

        public NewPostoPage()
        {
            InitializeComponent();
            Posto = new Posto { Nome = "Nome do posto", Endereco="Endereço do posto" };
            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "AddPosto", Posto);
            await Navigation.PopToRootAsync();
        }
    }
}
