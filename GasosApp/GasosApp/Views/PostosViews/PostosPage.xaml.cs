﻿using GasosApp.Models;
using GasosApp.ViewModels.PostoViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GasosApp.Views.PostosViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostosPage : ContentPage
    {
        PostosViewModel viewModel;
        public PostosPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new PostosViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var posto = args.SelectedItem as Posto;
            if (posto == null)
                return;
            Debug.WriteLine(posto.Id);
            await Navigation.PushAsync(new PostoDetailPage(new PostoDetailViewModel(posto)));

            // Manually deselect item
            PostosListView.SelectedItem = null;

        }
        

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewPostoPage());
        }

        protected override void OnAppearing()
        {
            
            base.OnAppearing();

            if (viewModel.Postos.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
