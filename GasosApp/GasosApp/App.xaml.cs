﻿using GasosApp.Views;
using GasosApp.Views.PostosViews;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace GasosApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            SetMainPage();
        }

        public static void SetMainPage()
        {
            Current.MainPage = new TabbedPage
            {
                Children =
                {
                    new NavigationPage(new PostosPage())
                    {
                        Title = "Lista de Postos",
                        Icon = Device.OnPlatform("tab_feed.png",null,null)
                    },
                    
                }
            };
        }
    }
}
